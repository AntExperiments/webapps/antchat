<?php
    $array = `tail -n 15 _messages.txt | tac`;
    $array = explode("\r\n", $array);

    for ($i = 0; $i <= count($array) - 1; $i++) {
        $message = explode(";", $array[$i]);
         
        echo "<div class=\"message\">";
        echo "<img src=\"{$message[2]}\">";
        echo "<span class=\"messageDate\">{$message[0]}</span>";
        echo "<span class=\"messageAuthor\">{$message[1]}</span><br>";
        echo "<span class=\"messageContent\">{$message[3]}</span>";
        echo "<div style=\"clear: both;\"></div>";
        echo "</div>";
    }
?>